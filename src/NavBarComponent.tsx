import * as React from 'react'

import { INavBarComponent } from './models/interfaces'
import * as S from './styles/NavStyles'

export class NavBarComponent extends React.Component<INavBarComponent> {
    public render(): React.ReactNode {
        const {
            elements,
            backgroundColor,
            textColor,
            hoverColor,
            hoverBackgroundColor,
        } = this.props

        return (
            <S.DivNavContainer backgroundColor={backgroundColor}>
                <S.DivNavInner>
                    {elements
                        ? elements.map(elem => (
                              <S.ANavElement
                                  href={elem.url}
                                  textColor={textColor}
                                  hoverColor={hoverColor}
                                  hoverBackgroundColor={hoverBackgroundColor}
                              >
                                  {elem.title}
                              </S.ANavElement>
                          ))
                        : ''}
                </S.DivNavInner>
            </S.DivNavContainer>
        )
    }
}
