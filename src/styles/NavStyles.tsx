import styled from 'styled-components'

import { IANavElement, INavContainer } from '../models/interfaces'

export const DivNavContainer = styled.div`
    background-color: ${(props: INavContainer) => `${props.backgroundColor}`};
    height: 50px;
`

export const DivNavInner = styled.div`
    display: flex;
    height: 100%;
    justify-content: space-around;
    margin: 0 auto;
    max-width: 1024px;
    width: 100%;
`

export const ANavElement = styled.a`
    :hover {
        background-color: ${(props: IANavElement) => `${props.hoverBackgroundColor}`};
        color: ${(props: IANavElement) => props.hoverColor};
    }
    align-items: center;
    color: ${(props: IANavElement) => props.textColor};
    display: flex;
    height: 100%;
    padding: 0 10px;
    text-align: center;
    text-decoration: none;
    text-transform: uppercase;
    transition: all 0.1s linear;
`
