import styled from 'styled-components'

import { IDivAirportsColumn, IProgressIndicator } from '../models/interfaces'

export const DivCard = styled.div`
    cursor: default;
    font-family: 'Open Sans';
    box-sizing: border-box;
    padding: 15px;
    height: 350px;
    width: 300px;
    color: white;
    font-weight: 100;
    background-image: linear-gradient(214deg, #19509b 13%, #607091 100%);
    transition: 0.5s ease-in-out;
`

// AIRPORT FROM TO STYLES
export const DivAirportsContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 20px;
`

export const DivAirportsColumn = styled.div`
    ${(props: IDivAirportsColumn) =>
        props.alignRight
            ? `
        text-align: right;
        margin-left: 2px;
    `
            : `
        text-align: left;
        margin-right: 2px;
    `};

    flex: 1 1;
    overflow: hidden;
`

export const H1 = styled.h1`
    font-weight: 100;
    font-size: 31px;
    margin: 0;
    white-space: nowrap;
    max-width: 100%;
    text-overflow: ellipsis;
    overflow: hidden;
`

export const H1Bold = styled.h1`
    font-weight: 700;
    font-size: 31px;
    margin: 0;
    white-space: nowrap;
    max-width: 100%;
    text-overflow: ellipsis;
    overflow: hidden;
`

export const SpanSmall = styled.span`
    font-size: 11px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: block;
`

// PROGRESS INDICATOR STYLES
export const DivProgressContainer = styled.div`
    margin-top: 35px;
    margin-left: 10px;
    margin-right: 10px;
`

export const DivProgressIndicator = styled.div`
    width: ${(props: IProgressIndicator) => (props.progress ? `${props.progress}%` : '100%')};
    background: rgba(255, 255, 255, 0.2);
    height: 10px;
    position: relative;
`

export const DivProgressIcon = styled.div`
    background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHdpZHRoPSIzMHB4IiBoZWlnaHQ9IjMwcHgiIHZpZXdCb3g9IjAgMCAzMCAzMCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj4gICAgICAgIDx0aXRsZT5haXJwbGFuZTwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZyBpZD0iUGFnZS0xIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxnIGlkPSJGbGlnaHRpbmZvLWNhcmQiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0xMTQuMDAwMDAwLCAtMTU0LjAwMDAwMCkiIGZpbGw9IiNGRkZGRkYiIHN0cm9rZT0iI0ZGRkZGRiI+ICAgICAgICAgICAgPGcgaWQ9ImFpcnBsYW5lIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMTUuMDAwMDAwLCAxNTUuMDAwMDAwKSI+ICAgICAgICAgICAgICAgIDxnIGlkPSJwcm9ncmVzcy1pbmRpY2F0b3IiPiAgICAgICAgICAgICAgICAgICAgPHBhdGggZD0iTTIuNzIwMDQ2NDFlLTE1LDE5LjA1MzA3ODYgTDIuMjQsMTQgTDIuNzIwMDQ2NDFlLTE1LDguOTQ2OTIxNDQgTDIuOCw4Ljk0NjkyMTQ0IEw1Ljk2NCwxMS45MTkzMjA2IEwxMS4wMDQsMTEuOTE5MzIwNiBMNy41MDQsMCBMMTAuMjc2LDAgTDE2LjU3NiwxMS45MTkzMjA2IEwyMy44LDExLjkxOTMyMDYgTDI0LjgzNiwxMS45Nzg3Njg2IEMyNS41NjQsMTIuMDE4NDAwNiAyNi4yNzMzMzMzLDEyLjIwNjY1MjUgMjYuOTY0LDEyLjU0MzUyNDQgQzI3LjY1NDY2NjcsMTIuODgwMzk2MyAyOCwxMy4zNjU4ODgyIDI4LDE0IEMyOCwxNC4zOTYzMTk5IDI3Ljg0MTMzMzMsMTQuNzMzMTkxOCAyNy41MjQsMTUuMDEwNjE1NyBDMjcuMjA2NjY2NywxNS4yODgwMzk2IDI2Ljg3MDY2NjcsMTUuNTA2MDE1NiAyNi41MTYsMTUuNjY0NTQzNSBDMjYuMTYxMzMzMywxNS44MjMwNzE1IDI1Ljc0MTMzMzMsMTUuOTIyMTUxNSAyNS4yNTYsMTUuOTYxNzgzNCBDMjQuNzcwNjY2NywxNi4wMDE0MTU0IDI0LjQzNDY2NjcsMTYuMDMxMTM5NCAyNC4yNDgsMTYuMDUwOTU1NCBDMjQuMDYxMzMzMywxNi4wNzA3NzE0IDIzLjkxMiwxNi4wODA2Nzk0IDIzLjgsMTYuMDgwNjc5NCBMMTYuNTc2LDE2LjA4MDY3OTQgTDEwLjI3NiwyOCBMNy41MDQsMjggTDExLjAwNCwxNi4wODA2Nzk0IEw1Ljk2NCwxNi4wODA2Nzk0IEwyLjgsMTkuMDUzMDc4NiBMMi43MjAwNDY0MWUtMTUsMTkuMDUzMDc4NiBaIiBmaWxsLXJ1bGU9Im5vbnplcm8iPjwvcGF0aD4gICAgICAgICAgICAgICAgPC9nPiAgICAgICAgICAgIDwvZz4gICAgICAgIDwvZz4gICAgPC9nPjwvc3ZnPg==);
    background-size: 100%;
    right: 0;
    width: 40px;
    height: 40px;
    position: absolute;
    transform: translateX(50%) translateY(-50%);
    top: 50%;
`

// ADDITIONAL INFO STYLES
export const SpanInfoTitle = styled.span`
    font-weight: 100;
`

export const SpanInfoValue = styled.span`
    font-weight: 600;
`

export const DivInfoContainer = styled.div`
    margin: 15px 0;
`

export const DivMoreInfoContainer = styled.div`
    margin-top: 40px;
`
