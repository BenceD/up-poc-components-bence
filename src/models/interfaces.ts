interface INavBarComponentElements {
    title: string
    url: string
}

// FLIGHT DATA COMPONENT
export interface IFlightDataComponent {
    altitude: number
    altitudeTitle: string
    destinationLong: string
    destinationShort: string
    loaded: boolean
    originLong: string
    originShort: string
    progress: number
    RemainingTitle: string
    speed: number
    speedTitle: string
    time: number
    title: string
}

export interface IDivAirportsColumn {
    alignRight?: boolean
}

export interface IProgressIndicator {
    progress: number
}

// NAVBAR COMPONENT
export interface INavBarComponent {
    backgroundColor: string
    elements?: [INavBarComponentElements]
    hoverBackgroundColor: string
    hoverColor: string
    textColor: string
}

export interface INavContainer {
    backgroundColor: string
}

export interface IANavElement {
    hoverBackgroundColor: string
    hoverColor: string
    href: string
    textColor: string
}
