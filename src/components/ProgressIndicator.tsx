import * as React from 'react'

import { IProgressIndicator } from '../models/interfaces'
import * as S from '../styles/FlightDataStyles'

export class ProgressIndicator extends React.Component<IProgressIndicator> {
    public render(): React.ReactNode {
        const { progress } = this.props

        return (
            <S.DivProgressContainer>
                <S.DivProgressIndicator progress={progress}>
                    <S.DivProgressIcon />
                </S.DivProgressIndicator>
            </S.DivProgressContainer>
        )
    }
}
