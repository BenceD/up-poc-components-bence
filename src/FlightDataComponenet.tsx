import * as React from 'react'

import { ProgressIndicator } from './components/ProgressIndicator'
import { IFlightDataComponent } from './models/interfaces'
import * as S from './styles/FlightDataStyles'

export class FlightDataComponent extends React.Component<IFlightDataComponent> {
    public render(): React.ReactNode {
        const {
            title,
            originLong,
            originShort,
            destinationLong,
            destinationShort,
            progress,
            RemainingTitle,
            time,
            altitudeTitle,
            altitude,
            speedTitle,
            speed,
            loaded,
        } = this.props

        const MINUTE_IN_SECONDS = 60
        const hours = Math.floor(time / MINUTE_IN_SECONDS)
        const timeHM = `${hours}h ${time - hours * MINUTE_IN_SECONDS}m`

        return (
            <S.DivCard>
                {/* TITLE */}
                <S.H1>{title}</S.H1>

                {loaded ? (
                    <>
                        {/* AIRPORT NAMES */}
                        <S.DivAirportsContainer>
                            {/* ORIGIN */}
                            <S.DivAirportsColumn>
                                <S.SpanSmall>{originLong}</S.SpanSmall>
                                <S.H1Bold>{originShort}</S.H1Bold>
                            </S.DivAirportsColumn>

                            {/* DESTINATION */}
                            <S.DivAirportsColumn alignRight>
                                <S.SpanSmall>{destinationLong}</S.SpanSmall>
                                <S.H1Bold>{destinationShort}</S.H1Bold>
                            </S.DivAirportsColumn>
                        </S.DivAirportsContainer>

                        {/* PROGRESS INDICATOR */}
                        <ProgressIndicator progress={progress} />

                        {/* ADDITIONAL INFORMATION */}
                        <S.DivMoreInfoContainer>
                            {/* REMAINING TIME */}
                            <S.DivInfoContainer>
                                <S.SpanInfoTitle>{RemainingTitle}</S.SpanInfoTitle>
                                <S.SpanInfoValue>{timeHM}</S.SpanInfoValue>
                            </S.DivInfoContainer>

                            {/* ALTITUDE */}
                            <S.DivInfoContainer>
                                <S.SpanInfoTitle>{altitudeTitle}</S.SpanInfoTitle>
                                <S.SpanInfoValue>{altitude} ft</S.SpanInfoValue>
                            </S.DivInfoContainer>

                            {/* SPEED */}
                            <S.DivInfoContainer>
                                <S.SpanInfoTitle>{speedTitle}</S.SpanInfoTitle>
                                <S.SpanInfoValue>{speed} mph</S.SpanInfoValue>
                            </S.DivInfoContainer>
                        </S.DivMoreInfoContainer>
                    </>
                ) : (
                    <>
                        {/* LOADING STATE */}
                        <span>Loading...</span>
                    </>
                )}
            </S.DivCard>
        )
    }
}
