import { FlightDataComponent } from '../src/index'
import * as React from 'react'
import renderer from 'react-test-renderer'

it('component should exist', () => {
    expect(FlightDataComponent).toBeDefined()
})

it('should render properly', () => {
    const wrapper = renderer.create(<FlightDataComponent />)
    expect(wrapper).toMatchSnapshot()
})
