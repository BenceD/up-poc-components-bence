import { boolean, number, select, text, color } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import * as React from 'react'
import { JssProvider } from 'react-jss'
import { createGlobalStyle } from 'styled-components'

import { NavBarComponent } from '../src/index'

import { generateClassName, jss } from './config/jss-config'

export const GlobalStyles = createGlobalStyle`
  body {
    @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800');
    font-family: 'Open Sans', sans-serif;
  }
`

const json01 = require('./jsons/json01.json')
const json02 = require('./jsons/json02.json')

const select1: any = {
    name: 'Json data:',
    options: {
        scenario01: json01,
        scenario02: json02,
    },
}

storiesOf('Navigation', module).add('Basic Loaded', () => (
    <>
        <GlobalStyles />
        <JssProvider jss={jss} generateClassName={generateClassName}>
            <NavBarComponent
                elements={select('Nav elem list', select1.options, select1.options.scenario01)}
                backgroundColor={color('Background Color', '#a7a1a1')}
                hoverBackgroundColor={color('Hover Background Color', 'rgba(0, 0, 0, 0.1)')}
                textColor={color('Text Color', '#fff')}
                hoverColor={color('Hover Color', '#cecece')}
            />
        </JssProvider>
    </>
))
