import { createGenerateClassName, jssPreset } from '@material-ui/core'
import { create, GenerateClassName, JSS } from 'jss'

const styleNode: Comment = document.createComment('insertion-point-carousel-jss')
document.head.insertBefore(styleNode, document.head.firstChild)

export const generateClassName: GenerateClassName = createGenerateClassName()

// TODO this should be added to the head
export const jss: JSS = create({
    ...jssPreset(),
    insertionPoint: 'insertion-point-carousel-jss',
})
