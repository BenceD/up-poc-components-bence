import { number, select, text, boolean } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import * as React from 'react'
import { JssProvider } from 'react-jss'
import { FlightDataComponent } from '../src/index'
import { generateClassName, jss } from './config/jss-config'
import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  body {
    @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800');
    font-family: 'Open Sans', sans-serif;
  }
`

storiesOf('Flight Data Card', module)
    .add('Basic Loaded', () => (
        <>
            <GlobalStyles />
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <FlightDataComponent
                    loaded={boolean('Loaded', true)}
                    title={text('Title', 'Flight information')}
                    originShort={text('Origin IATA', 'BUD')}
                    originLong={text('Origin name', 'Budapest')}
                    destinationShort={text('Destination IATA', 'LAX')}
                    destinationLong={text('Destination name', 'Los Angeles')}
                    progress={number('Percentage', 40)}
                    RemainingTitle={text('Remaining time title', 'Remaining time: ')}
                    time={number('Remaing time in minutes', 100)}
                    altitudeTitle={text('Altitude title', 'Altitude: ')}
                    altitude={number('Altitude in feet', 1000)}
                    speedTitle={text('Speed title', 'Speed: ')}
                    speed={number('Speed in mph', 473)}
                />
            </JssProvider>
        </>
    ))
    .add('Basic Loading', () => (
        <>
            <GlobalStyles />
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <FlightDataComponent
                    loaded={boolean('Loaded', false)}
                    title={text('Title', 'Flight information')}
                    originShort={text('Origin IATA', 'BUD')}
                    originLong={text('Origin name', 'Budapest')}
                    destinationShort={text('Destination IATA', 'LAX')}
                    destinationLong={text('Destination name', 'Los Angeles')}
                    progress={number('Percentage', 40)}
                    RemainingTitle={text('Remaining time title', 'Remaining time: ')}
                    time={number('Remaing time in minutes', 100)}
                    altitudeTitle={text('Altitude title', 'Altitude: ')}
                    altitude={number('Altitude in feet', 1000)}
                    speedTitle={text('Speed title', 'Speed: ')}
                    speed={number('Speed in mph', 473)}
                />
            </JssProvider>
        </>
    ))
    .add('Long airport names', () => (
        <>
            <GlobalStyles />
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <FlightDataComponent
                    loaded={boolean('Loaded', true)}
                    title={text('Title', 'Flight information')}
                    originShort={text('Origin IATA', 'ZIA')}
                    originLong={text('Origin name', 'Zhukovsky International Airport')}
                    destinationShort={text('Destination IATA', 'ULY')}
                    destinationLong={text('Destination name', 'Ulyanovsk Vostochny Airport')}
                    progress={number('Percentage', 40)}
                    RemainingTitle={text('Remaining time title', 'Remaining time: ')}
                    time={number('Remaing time in minutes', 100)}
                    altitudeTitle={text('Altitude title', 'Altitude: ')}
                    altitude={number('Altitude in feet', 1000)}
                    speedTitle={text('Speed title', 'Speed: ')}
                    speed={number('Speed in mph', 473)}
                />
            </JssProvider>
        </>
    ))
    .add('Long card title', () => (
        <>
            <GlobalStyles />
            <JssProvider jss={jss} generateClassName={generateClassName}>
                <FlightDataComponent
                    loaded={boolean('Loaded', true)}
                    title={text('Title', 'Flight information for passengers')}
                    originShort={text('Origin IATA', 'BUD')}
                    originLong={text('Origin name', 'Budapest')}
                    destinationShort={text('Destination IATA', 'LAX')}
                    destinationLong={text('Destination name', 'Los Angeles')}
                    progress={number('Percentage', 40)}
                    RemainingTitle={text('Remaining time title', 'Remaining time: ')}
                    time={number('Remaing time in minutes', 100)}
                    altitudeTitle={text('Altitude title', 'Altitude: ')}
                    altitude={number('Altitude in feet', 1000)}
                    speedTitle={text('Speed title', 'Speed: ')}
                    speed={number('Speed in mph', 473)}
                />
            </JssProvider>
        </>
    ))
