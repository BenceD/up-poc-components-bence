var path = require('path')

module.exports = (env, options) => {
    const isDevMode = options.mode === 'development'

    return {
        devtool: isDevMode ? 'source-map' : false,
        entry: './src/index.ts',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'index.js',
            libraryTarget: 'umd',
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.json'],
        },
        module: {
            rules: [
                {
                    test: /\.m?(ts|js)x?$/,
                    include: path.resolve(__dirname, 'src'),
                    exclude: /(node_modules|dist)/,
                    use: {
                        loader: 'babel-loader',
                    },
                },
            ],
        },
        externals: {
            react: 'react',
            'react-dom': 'react-dom',
            'react-router': 'react-router',
            '@material-ui/core': '@material-ui/core',
        },
    }
}
